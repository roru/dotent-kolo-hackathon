﻿using System;
using Kolo.API.Data.Entities;

namespace Kolo.API.Models
{
    public class HistoryRecordViewModel
    {
        public int Id { get; set; }
        public int SellerId { get; set; }
        public string SellerName { get; set; }
        public DateTimeOffset Date { get; set; }

        public static HistoryRecordViewModel CreateFrom(HistroryRecord entity)
        {
            return new HistoryRecordViewModel
            {
                Id = entity.Id,
                SellerId = entity.OriginSellerId,
                SellerName = FullName(entity.OriginSeller),
                Date = entity.CreatedDate
            };
        }

        public static string FullName(Seller entity)
            => $"{entity.FirstName} {entity.LastName}";
    }
}
