﻿using System;
using System.Linq;
using Kolo.API.Data.Entities;

namespace Kolo.API.Models
{
    public class SellerViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public DocumentViewModel[] Documents { get; set; }

        public static SellerViewModel CreateFrom(Seller entity)
        {
            return new SellerViewModel
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Description = entity.Description,
                Address = entity.Address,
                Documents = entity.Documents.OrderBy(d => d.Id).Select(DocumentViewModel.CreateFrom).ToArray()
            };
        }
    }
}
