﻿using System;
using Kolo.API.Data.Entities;

namespace Kolo.API.Models
{
    public class DocumentViewModel
    {
        public int Id { get; private set; }
        public string FileName { get; private set; }

        public static DocumentViewModel CreateFrom(Document entity)
        {
            return new DocumentViewModel
            {
                Id = entity.Id,
                FileName = entity.FileName
            };
        }
    }
}