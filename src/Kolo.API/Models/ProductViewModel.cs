﻿using System;
using System.Linq;
using Kolo.API.Data.Entities;

namespace Kolo.API.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int HarvestYear { get; set; }
        public string Kind { get; set; }
        public double QuantetyKG { get; set; }
        public int BatchNumber { get; private set; }
        public int SelerId { get; set; }
        public DocumentViewModel[] Documents { get; set; }

        public static ProductViewModel CreateFrom(Product entity)
        {
            return new ProductViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                HarvestYear = entity.HarvestDate.Year,
                Kind = entity.Kind,
                QuantetyKG = entity.QuantetyKG,
                SelerId = entity.FarmerId,
                BatchNumber = entity.BatchNumber,
                Documents = entity.Documents.OrderBy(d => d.Id).Select(DocumentViewModel.CreateFrom).ToArray()
            };
        }
    }
}
