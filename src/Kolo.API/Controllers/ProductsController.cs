﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Kolo.API.Data;
using Kolo.API.Data.Entities;
using Kolo.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Kolo.API.Controllers
{
    public class ProductsController : ApiControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IRepository<Product> _productRepo;

        public ProductsController(
            ILogger<ProductsController> logger,
            IRepository<Product> productRepo)
        {
            _logger = logger;
            _productRepo = productRepo;
        }

        [ProducesResponseType(typeof(ProductViewModel[]), 200)]
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var producst = await _productRepo
                .GetAllAsync()
                .ConfigureAwait(false);

            return Ok(producst.Select(ProductViewModel.CreateFrom));
        }
    }
}
