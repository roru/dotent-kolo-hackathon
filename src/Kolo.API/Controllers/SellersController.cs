﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Kolo.API.Data;
using Kolo.API.Data.Entities;
using Kolo.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

namespace Kolo.API.Controllers
{
    public class SellersController : ApiControllerBase
    {
        private readonly ILogger<SellersController> _logger;
        private readonly IRepository<Seller> _sellerRepo;
        private readonly KoloDbContext _dbContext;

        public SellersController(
            ILogger<SellersController> logger,
            IRepository<Seller> farmerRepo,
            KoloDbContext dbContext)
        {
            _logger = logger;
            _sellerRepo = farmerRepo;
            this._dbContext = dbContext;
        }

        [ProducesResponseType(typeof(SellerViewModel[]), 200)]
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var entities = await _sellerRepo
                .GetAllAsync()
                .ConfigureAwait(false);

            return Ok(entities.Select(SellerViewModel.CreateFrom));
        }

        [ProducesResponseType(typeof(SellerViewModel), 200)]
        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(int id)
        {
            Seller seller = await GetSellerById(id)
                .ConfigureAwait(false);

            if (seller == null)
            {
                return NotFound();
            }

            return Ok(SellerViewModel.CreateFrom(seller));
        }

        [HttpPost("{id}/documents")]
        public async Task<ActionResult> AddSellerCertificate(
            [FromServices]IRepository<Document> docRepo,
            [FromRoute]int id,
            IFormFile file)
        {
            var seller = await GetSellerById(id)
                .ConfigureAwait(false);

            if (seller == null)
            {
                return NotFound();
            }

            var newDocument = docRepo.Create();

            newDocument.SellerId = seller.Id;
            newDocument.FileName = file.FileName;

            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms)
                    .ConfigureAwait(false);

                newDocument.Content = ms.ToArray();
            }

            await docRepo.Update(newDocument)
                .ConfigureAwait(false);

            await _dbContext.SaveChangesAsync()
                .ConfigureAwait(false);

            return Ok();
        }

        [HttpDelete("{id}/documents/{docId}")]
        public async Task<ActionResult> RemoveSellerCertificate(
            [FromServices]IRepository<Document> docRepo,
            [FromRoute]int id,
            [FromRoute]int docId)
        {
            var seller = await GetSellerById(id)
                .ConfigureAwait(false);

            if (seller == null)
            {
                return NotFound();
            }

            var doc = seller.Documents.FirstOrDefault(d => d.Id == docId);
            if (doc == null)
            {
                return NotFound();
            }

            await docRepo.Remove(doc)
                .ConfigureAwait(false);

            await _dbContext.SaveChangesAsync()
                .ConfigureAwait(false);

            return NoContent();
        }

        [ProducesResponseType(typeof(ProductViewModel[]), 200)]
        [HttpGet("{id}/products")]
        public async Task<ActionResult> GetProducts(
            [FromRoute] int id)
        {
            var seller = await GetSellerById(id)
                .ConfigureAwait(false);

            if (seller == null)
            {
                return NotFound();
            }

            var products = seller.Products.Select(ProductViewModel.CreateFrom);
            return Ok(products);
        }

        [HttpPost("{id}/products/{productId}/documents")]
        public async Task<ActionResult> SetProducCertificate(
            [FromServices]IRepository<Document> docRepo,
            [FromRoute]int id,
            [FromRoute]int productId,
            [Required]IFormFile file)
        {
            var seller = await GetSellerById(id)
               .ConfigureAwait(false);

            if (seller == null)
            {
                return NotFound();
            }

            var product = seller.Products.FirstOrDefault(p => p.Id == productId);

            if (product == null)
            {
                return NotFound();
            }

            var newDocument = docRepo.Create();

            newDocument.ProductId = product.Id;
            newDocument.FileName = file.FileName;

            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms)
                    .ConfigureAwait(false);

                newDocument.Content = ms.ToArray();
            }

            await docRepo.Update(newDocument)
                .ConfigureAwait(false);

            await _dbContext.SaveChangesAsync()
                .ConfigureAwait(false);

            return Ok();
        }

        [HttpDelete("{id}/products/{productId}/documents/{docId}")]
        public async Task<ActionResult> DeleteProducCertificate(
            [FromServices]IRepository<Document> docRepo,
            [FromRoute]int id,
            [FromRoute]int productId,
            [FromRoute]int docId)
        {
            var seller = await GetSellerById(id)
               .ConfigureAwait(false);

            if (seller == null)
            {
                return NotFound();
            }

            var product = seller.Products.FirstOrDefault(p => p.Id == productId);

            if (product == null)
            {
                return NotFound();
            }

            var doc = product.Documents.FirstOrDefault(d => d.Id == docId);
            if (doc == null)
            {
                return NotFound();
            }

            await docRepo.Remove(doc)
                .ConfigureAwait(false);

            await _dbContext.SaveChangesAsync()
                .ConfigureAwait(false);

            return Ok();
        }

        [ProducesResponseType(typeof(HistoryRecordViewModel[]), 200)]
        [HttpGet("{id}/products/{productId}/history")]
        public async Task<ActionResult> GetProductHistory(
            [FromRoute]int id,
            [FromRoute]int productId)
        {
            var seller = await GetSellerById(id)
                .ConfigureAwait(false);

            if (seller == null)
            {
                return NotFound();
            }

            var product = seller.Products.FirstOrDefault(p => p.Id == productId);

            if (product == null)
            {
                return NotFound();
            }

            var history = product.Histrory
                .OrderByDescending(r => r.CreatedDate)
                .Select(HistoryRecordViewModel.CreateFrom);

            return Ok(history);
        }


        private Task<Seller> GetSellerById(int id) =>
            _sellerRepo.FindSingleAsync(e => e.Id == id);
    }
}
