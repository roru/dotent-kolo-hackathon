﻿using System;
using System.IO;
using System.Threading.Tasks;
using Kolo.API.Data;
using Kolo.API.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using MimeTypes.Core;

namespace Kolo.API.Controllers
{
    public class DocumentsController : ApiControllerBase
    {
        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(
            [FromRoute]int id,
            [FromServices]IRepository<Document> docRepo)
        {
            var document = await docRepo
                .FindSingleAsync(d => d.Id == id)
                .ConfigureAwait(false);

            if (document == null)
            {
                return NotFound();
            }

            return File(document.Content, MimeTypeMap.GetMimeType(Path.GetExtension(document.FileName)));
        }
    }
}
