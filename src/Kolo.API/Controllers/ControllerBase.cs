﻿using System;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Kolo.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [EnableCors]
    public abstract class ApiControllerBase : ControllerBase
    {
        public ApiControllerBase()
        {
        }
    }
}
