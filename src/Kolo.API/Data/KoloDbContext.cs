﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Kolo.API.Data
{
    public class KoloDbContext : DbContext
    {
        public KoloDbContext(DbContextOptions<KoloDbContext> options)
            : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(KoloDbContext).Assembly);
        }
    }
}
