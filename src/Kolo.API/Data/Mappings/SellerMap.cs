﻿using System;
using Kolo.API.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kolo.API.Data.Mappings
{
    public class SellerMap : IEntityTypeConfiguration<Seller>
    {
        public void Configure(EntityTypeBuilder<Seller> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();

            builder.Property(e => e.FirstName);
            builder.Property(e => e.LastName);
            builder.Property(e => e.Address);
            builder.Property(e => e.Description);

            builder.HasMany(e => e.Products)
                .WithOne(p => p.Seller)
                .HasForeignKey(e => e.FarmerId);

            builder.HasMany(e => e.SoldRecords)
                .WithOne(o => o.OriginSeller)
                .HasForeignKey(e => e.OriginSellerId);

            builder.HasMany(e => e.BoughtRecords)
                .WithOne(o => o.Seller)
                .HasForeignKey(e => e.SellerId);

            builder.HasMany(e => e.Documents)
                .WithOne(d => d.Seller)
                .HasForeignKey(e => e.SellerId);
        }
    }
}
