﻿using System;
using Kolo.API.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kolo.API.Data.Mappings
{
    public class ProductMap : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Property(e => e.Name);
            builder.Property(e => e.FarmerId);
            builder.Property(e => e.HarvestDate);
            builder.Property(e => e.QuantetyKG);
            builder.Property(e => e.BatchNumber);

            builder.HasMany(e => e.Histrory)
                .WithOne(r => r.Product)
                .HasForeignKey(e => e.ProductId);

            builder.HasMany(e => e.Documents)
                .WithOne(d => d.Product)
                .HasForeignKey(e => e.ProductId);

        }
    }
}
