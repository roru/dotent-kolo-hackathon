﻿using System;
using Kolo.API.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kolo.API.Data.Mappings
{
    public class HistoryRecordMap : IEntityTypeConfiguration<HistroryRecord>
    {
        public void Configure(EntityTypeBuilder<HistroryRecord> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ProductId);
            builder.Property(e => e.SellerId);
            builder.Property(e => e.OriginSellerId);
            builder.Property(e => e.CreatedDate);
        }
    }
}
