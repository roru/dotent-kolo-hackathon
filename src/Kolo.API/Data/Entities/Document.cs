﻿using System;
namespace Kolo.API.Data.Entities
{
    public class Document : EntityBase
    {
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        public int SellerId { get; set; }
        public int ProductId { get; set; }

        public virtual Seller Seller { get; set; }
        public virtual Product Product { get; set; }
    }
}
