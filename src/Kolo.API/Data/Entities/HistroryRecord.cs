﻿using System;
namespace Kolo.API.Data.Entities
{
    public class HistroryRecord : EntityBase
    {
        public int ProductId { get; set; }
        public int OriginProductId { get; set; }
        public int SellerId { get; set; }
        public int OriginSellerId { get; set; }
        public DateTimeOffset CreatedDate { get; set; }

        public virtual Product Product { get; set; }
        public virtual Product OriginProduct { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Seller OriginSeller { get; set; }
    }
}
