﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Kolo.API.Data.Entities
{
    public class Seller : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Document> Documents { get; set; } = new HashSet<Document>();
        public virtual ICollection<Product> Products { get; set; } = new HashSet<Product>();
        public virtual ICollection<HistroryRecord> SoldRecords { get; set; } = new HashSet<HistroryRecord>();
        public virtual ICollection<HistroryRecord> BoughtRecords { get; set; } = new HashSet<HistroryRecord>();
    }
}
