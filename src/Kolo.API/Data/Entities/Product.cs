﻿using System;
using System.Collections.Generic;

namespace Kolo.API.Data.Entities
{
    public class Product : EntityBase
    {
        public string Name { get; set; }
        public int FarmerId { get; set; }
        public DateTimeOffset HarvestDate { get; set; }
        public string Kind { get; set; }
        public double QuantetyKG { get; set; }
        public int BatchNumber { get; set; }

        public virtual Seller Seller { get; set; }
        public virtual ICollection<HistroryRecord> Histrory { get; set; }
        public virtual ICollection<Document> Documents { get; set; } = new HashSet<Document>();
    }
}
