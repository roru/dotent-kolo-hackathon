﻿using System;
using System.IO;
using System.Reflection.Metadata;
using Kolo.API.Data.Entities;
using Microsoft.EntityFrameworkCore.Internal;
using Document = Kolo.API.Data.Entities.Document;

namespace Kolo.API.Data
{
    public static class KoloHackathonDbInitializer
    {
        public static void Initialize(KoloDbContext context)
        {
            context.Database.EnsureCreated();

            var farmers = new[]
            {
                new Seller {Id = 1, FirstName = "Taras", LastName = "Bulba", Address = "Арабка, Zaporizhia Oblast, Ukraine", Description = "Сerfied Farmer and entrepreneur in Ukraine"},
                new Seller {Id = 2, FirstName = "Bogdan", LastName = "Khmelnitsky", Address = "Ukraine", Description = "Сerfied entrepreneur in Ukraine"},
                new Seller {Id = 3, FirstName = "Martin", LastName = "Borulya", Address = "Ukraine", Description = "Сerfied Farmer in Ukraine"},

                new Seller {Id = 5, FirstName = "Zaporizhzhya", LastName = "Sich", Address = "Ukraine, Zaporizhzhia", Description = "Big retailer in Ukraine"}
            };
            var farmersTable = context.Set<Seller>();
            foreach (var f in farmers)
            {
                farmersTable.Add(f);
            }
            context.SaveChanges();

            var products = new[]
            {
                new Product {Id = 30, BatchNumber = 2, FarmerId = 1, Name = "Honey", QuantetyKG = 50, Kind = "lime", HarvestDate =  DateTimeOffset.Now.AddMonths(-5) },

                new Product{Id = 1, BatchNumber = 20, FarmerId=1, Name="Bulba", Kind = "", QuantetyKG = 100, HarvestDate = DateTimeOffset.Now.AddYears(-1)},
                new Product{Id = 10, BatchNumber = 30,FarmerId=1, Name="Bulba", Kind = "", QuantetyKG = 100, HarvestDate = DateTimeOffset.Now.AddYears(-1)},

                new Product{Id = 2, BatchNumber = 15, FarmerId=1, Name="Wheat", Kind = "", QuantetyKG = 1000, HarvestDate = DateTimeOffset.Now.AddYears(-2)},

                new Product{Id = 3, BatchNumber = 40,FarmerId=1, Name="beet", Kind = "Chioggia", QuantetyKG = 70, HarvestDate = DateTimeOffset.Now},

                
                new Product{Id = 4, BatchNumber = 25, FarmerId=2, Name="beet", Kind = "Red", QuantetyKG = 30, HarvestDate = DateTimeOffset.Now.AddYears(-1)},

                
                new Product{Id = 5, BatchNumber = 46, FarmerId=3, Name="Bulba", Kind = "", QuantetyKG = 140, HarvestDate = DateTimeOffset.Now},
                new Product{Id = 6, BatchNumber = 58, FarmerId=3, Name="beet", Kind = "Red", QuantetyKG = 3000, HarvestDate = DateTimeOffset.Now.AddYears(-1)},

                
                new Product{Id = 7, BatchNumber = 75, FarmerId=5, Name="beet", Kind = "Red", QuantetyKG = 1030, HarvestDate = DateTimeOffset.Now.AddYears(-1)},
                new Product{Id = 8, BatchNumber = 85, FarmerId=5, Name="Bulba", Kind = "", QuantetyKG = 4000, HarvestDate = DateTimeOffset.Now},
            };
            var productTable = context.Set<Product>();
            foreach (var p in products)
            {
                productTable.Add(p);
            }
            context.SaveChanges();

            var history = new[]
            {
                new HistroryRecord{ProductId = 7, OriginProductId = 3, CreatedDate = DateTimeOffset.Now.AddMonths(-3), OriginSellerId = 3, SellerId = 5 },
                new HistroryRecord{ProductId = 8, OriginProductId = 1, CreatedDate = DateTimeOffset.Now.AddMonths(-4), OriginSellerId = 1, SellerId = 5 },
                new HistroryRecord{ProductId = 8, OriginProductId = 5, CreatedDate = DateTimeOffset.Now.AddMonths(-4), OriginSellerId = 3, SellerId = 5 },
            };
            var histotyTable = context.Set<HistroryRecord>();
            histotyTable.AddRange(history);
            context.SaveChanges();

            var documents = new[]
            {
                new Document
                {
                    SellerId = 1,
                    FileName = "Organic Cer.pdf",
                    Content = File.ReadAllBytes("Documents/Organic Cer.pdf")
                },
                new Document
                {
                    SellerId = 1,
                    FileName = "Veterinary Seller Certificate.jpg",
                    Content = File.ReadAllBytes("Documents/Veterinary Seller Certificate.jpg")
                },

                new Document
                {
                    ProductId = 30,
                    FileName = "Lab Product Summary.jpg",
                    Content = File.ReadAllBytes("Documents/Lab Product Summary.jpg")
                },
                new Document
                {
                    ProductId = 30,
                    FileName = "Veterinary Certificate.jpg",
                    Content = File.ReadAllBytes("Documents/Veterinary Certificate.jpg")
                },
                new Document
                {
                    ProductId = 30,
                    FileName = "Lab Environment Summary.jpg",
                    Content = File.ReadAllBytes("Documents/Lab Environment Summary.jpg")
                },
            };
            var documentTabels = context.Set<Document>();
            documentTabels.AddRange(documents);
            context.SaveChanges();
        }
    }
}