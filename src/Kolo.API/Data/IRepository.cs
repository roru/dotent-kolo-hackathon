﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Kolo.API.Data
{
    public interface IRepository<TEntity>
        where TEntity : EntityBase
    {
        Task<TEntity[]> GetAllAsync();
        Task<TEntity> FindSingleAsync(Expression<Func<TEntity, bool>> condition);
        Task<TEntity[]> FindManyAsync(Expression<Func<TEntity, bool>> condition);
        Task Update(TEntity entity);
        TEntity Create();
        Task Remove(TEntity entity);
    }
}
