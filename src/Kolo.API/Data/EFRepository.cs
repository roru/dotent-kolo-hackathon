using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Kolo.API.Data
{
    public class EFRepository<TEntity> : IRepository<TEntity>
        where TEntity : EntityBase
    {
        private readonly DbContext _dbContext;

        public EFRepository(KoloDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<TEntity[]> GetAllAsync()
        {
            return _dbContext.Set<TEntity>().ToArrayAsync();
        }

        public Task<TEntity> FindSingleAsync(
            Expression<Func<TEntity, bool>> condition)
        {
            return _dbContext.Set<TEntity>().FirstOrDefaultAsync(condition);
        }

        public Task<TEntity[]> FindManyAsync(
            Expression<Func<TEntity, bool>> condition)
        {
            return _dbContext.Set<TEntity>().Where(condition).ToArrayAsync();
        }

        public Task Update(TEntity entity)
        {
            _dbContext.Update(entity);
            return Task.CompletedTask;
        }

        public TEntity Create() => _dbContext.CreateProxy<TEntity>();

        public Task Remove(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            return Task.CompletedTask;
        }
    }
}
